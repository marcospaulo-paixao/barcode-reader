import { NoCameraAccess } from "@modules/Scanner/NoCameraAccess";
import { RequestingCameraPermission } from "@modules/Scanner/RequestingCameraPermission";
import { IconChevronLeft } from "@shared/Icons";
import { styles } from "@shared/Styles";
import { Button, Text } from "@ui-kitten/components";
import { BarCodeScanner } from "expo-barcode-scanner";
import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";

const ACCEPTED_STATUS = "granted";

export const Scanner = ({ navigation, route }: any) => {
  const [hasPermission, setHasPermission] = useState<any>(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    const getBarCodeScannerPermissions = async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === ACCEPTED_STATUS);
    };
    getBarCodeScannerPermissions().then(() => {});
  }, []);

  const handleCompleteCodeScanned = ({ type, data }: any) => {
    const { component } = route.params;
    if (component !== undefined) {
      navigate(component.name, { type: type, cod: data });
      return;
    }
    navigation.pop();
  };

  const navigate = (
    componentName: string,
    codeComplete: { type: any; cod: any }
  ) => {
    navigation.navigate(componentName, {
      data: codeComplete,
    });
  };

  const navigatePop = () => {
    const { component } = route.params;
    if (component !== undefined) {
      navigate(component.name, { type: null, cod: null });
      return;
    }
    navigation.pop();
  };

  const handleBarCodeScanned = (result: any) => {
    setScanned(true);
    setTimeout(() => {
      handleCompleteCodeScanned(result);
    }, 500);
  };

  return (
    <View style={[styles.flex1, styles.m10]}>
      <View>
        <View
          style={[styles.flexRow, styles.justifyContentStart, styles.wFull]}
        >
          <Button
            onPress={() => navigatePop()}
            accessoryLeft={IconChevronLeft}
            style={[styles.width150]}
          >
            {(evaProps) => <Text {...evaProps}>Voltar</Text>}
          </Button>
        </View>
      </View>
      <View style={[styles.flex1, styles.mt5, styles.mb5]}>
        {hasPermission === null && <RequestingCameraPermission />}
        {hasPermission === false && <NoCameraAccess />}
        {hasPermission !== null && hasPermission === true && (
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />
        )}
      </View>
    </View>
  );
};
