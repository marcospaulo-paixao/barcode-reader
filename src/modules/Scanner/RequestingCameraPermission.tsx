import { Text } from "@ui-kitten/components";
import React from "react";
import { View } from "react-native";

import { styles } from "@shared/Styles";

export const RequestingCameraPermission = () => (
  <View style={styles.containerCenter}>
    <Text>Solicitando permissão de câmera</Text>
  </View>
);
