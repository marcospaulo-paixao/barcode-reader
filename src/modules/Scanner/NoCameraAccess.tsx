import { styles } from "@shared/Styles";
import { Text } from "@ui-kitten/components";
import React from "react";
import { View } from "react-native";

export const NoCameraAccess = () => (
  <View style={styles.containerCenter}>
    <Text>Sem acesso à câmera</Text>
  </View>
);
