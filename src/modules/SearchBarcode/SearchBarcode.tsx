import { BarCodeInfo } from "@modules/SearchBarcode/BarCodeInfo";
import { useFocusEffect } from "@react-navigation/native";
import { IconBarcode, styles } from "@shared/index";
import { Button } from "@ui-kitten/components";
import React, { useState } from "react";
import { View } from "react-native";

export const SearchBarcode = ({ navigation, route }: any) => {
  const [type, setType] = useState<any>(null);
  const [cod, setCod] = useState<any>(null);

  useFocusEffect(
    React.useCallback(() => {
      const cod = route.params?.data?.cod!;
      if (cod) {
        const { type, cod } = route.params.data;
        setType(type);
        setCod(cod);
      } else if (cod === null) {
        setType(null);
        setCod(null);
      }
    }, [setType, setCod, route])
  );

  const navigateScanner = () => {
    navigation.navigate("Scanner", {
      component: { name: "SearchBarcode" },
    });
  };

  return (
    <View style={[styles.flex1, styles.m10]}>
      <View>
        <Button
          style={[styles.width150]}
          onPress={navigateScanner}
          accessoryLeft={IconBarcode}
        >
          Consultar
        </Button>
      </View>
      {cod !== null && <BarCodeInfo cod={cod} type={type} />}
    </View>
  );
};
