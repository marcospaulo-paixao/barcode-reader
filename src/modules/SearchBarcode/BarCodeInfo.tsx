import { styles } from "@shared/Styles";
import { Text } from "@ui-kitten/components";
import React from "react";
import { View } from "react-native";

export const BarCodeInfo = ({ type, cod }: any) => (
  <View>
    <View style={[styles.flexRow, styles.mt10]}>
      <Text style={styles.fontWeightBold}>Tipo: </Text>
      <Text>{type}</Text>
    </View>
    <View style={[styles.flexRow, styles.w85, styles.mt10]}>
      <Text style={styles.fontWeightBold}>Código: </Text>
      <Text>{cod}</Text>
    </View>
  </View>
);
