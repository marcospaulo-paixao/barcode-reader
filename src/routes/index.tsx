import { Scanner, SearchBarcode } from "@modules/index";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";

const { Navigator, Screen } = createStackNavigator();

const ROUTE_NAVIGATOR = [
  { name: "SearchBarcode", component: SearchBarcode },
  { name: "Scanner", component: Scanner },
];

export const AppNavigator = () => (
  <NavigationContainer>
    <Navigator screenOptions={{ headerShown: false }}>
      {ROUTE_NAVIGATOR.map((route, index) => {
        return (
          <Screen key={index} name={route.name} component={route.component} />
        );
      })}
    </Navigator>
  </NavigationContainer>
);
