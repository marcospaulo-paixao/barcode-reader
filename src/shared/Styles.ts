import { StyleSheet, ViewStyle } from "react-native";

const justifyContentCenter: ViewStyle = {
  justifyContent: "center",
};
const alignItemsCenter: ViewStyle = {
  alignItems: "center",
};
const flex1: ViewStyle = {
  flex: 1,
};

export const styles = StyleSheet.create({
  flex1: {
    ...flex1,
  },
  flexRow: {
    flexDirection: "row",
  },
  justifyContentSpaceBetween: {
    justifyContent: "space-between",
  },
  justifyContentSpaceAround: {
    justifyContent: "space-around",
  },
  justifyContentStart: {
    justifyContent: "flex-start",
  },
  alignItemsCenter: {
    ...alignItemsCenter,
  },
  justifyContentCenter: {
    ...justifyContentCenter,
  },
  containerCenter: {
    ...flex1,
    ...alignItemsCenter,
    ...justifyContentCenter,
  },
  m10: {
    margin: 10,
  },
  m5: {
    margin: 5,
  },
  ml20: {
    marginLeft: 20,
  },
  mt5: {
    marginTop: 5,
  },
  mb5: {
    marginBottom: 5,
  },
  mt10: {
    marginTop: 10,
  },
  wFull: {
    width: "100%",
  },
  width150: {
    width: 150,
  },
  w85: {
    width: "85%",
  },
  fontWeightBold: {
    fontWeight: "bold",
  },
});

console.log(styles.containerCenter);
