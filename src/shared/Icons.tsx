import FontAwesome from "@expo/vector-icons/FontAwesome";
import React from "react";

export const IconChevronLeft = () => (
  <FontAwesome name="chevron-left" size={32} color="white" />
);

export const IconBarcode = () => (
  <FontAwesome name="barcode" size={32} color="white" />
);
